#include "point.h"

Point::Point(int x, int y) {
  _x = x;
  _y = y;
}

int Point::getX() {
  return _x;
}

int Point::getY() {
  return _y;
}

void Point::setX(int x) {
  _x = x;
}

void Point::setY(int y) {
  _y = y;
}

float Point::distance(Point p) {
  return sqrt(((p.getX() - _x) * (p.getX() - _x)) + ((p.getY() - _y) * (p.getY() - _y)));
}
