#include "robot.h"

Robot::Robot(Camera &cam) : camera(cam)
{
	bodyX = -0.5; headX = -0.5;
	bodyY = 0.1875; headY = 0.4375;
	bodyZ = -0.5; headZ = -0.5;
	angle = 0;
	headAngle = bodyAngle = 0;
	countUp = countLeft = 0;
	sCount = dCount = 0;
	facingLeft = facingBack = facingRight = false;
	facingForward = true;
}

void Robot::drawFilledCircle(GLfloat x, GLfloat y, GLfloat z, GLfloat radius){
	int i;
	int triangleAmount = 20;
	
	GLfloat twicePi = 2.0f * PI;
	
	glBegin(GL_TRIANGLE_FAN);
		glVertex3f(x, y, z);
		for(i = 0; i <= triangleAmount;i++) { 
			glVertex3f(
		            x + (radius * cos(i *  twicePi / triangleAmount)), 
			    y + (radius * sin(i * twicePi / triangleAmount)), z
			);
		}
	glEnd();
}

void Robot::renderRobot()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(bodyX, bodyY, bodyZ);

	gluLookAt(camera.getX(), camera.getY(), camera.getZ(),
        	camera.getAtX(), camera.getAtY(), camera.getAtZ(),
            camera.getUpX(), camera.getUpY(), camera.getUpZ());
	// BODY
    glPushMatrix();
	glColor3f(0.184f,0.309f,0.309f);
	glScalef(1.0, 1.5, 1.0);
    glutSolidCube((GLdouble) 0.25);
   	glPopMatrix();

	// HEAD
    glPushMatrix();
    glColor3f(0.3f,0.5f,0.5f);
    glRotatef(headAngle, 0, 1, 0);
    glTranslatef(0, 0.3, 0);
    glutSolidCube((GLdouble) 0.125);
    glPopMatrix();
    
	// ANTENNA
    glPushMatrix();
    GLUquadricObj * qobj = gluNewQuadric();
    glColor3f(0.5f,0.7f,0.7f);
    glTranslatef(0.0, 0.425, 0.0);
	glRotatef(angle, 0.0f, 1.0f, 0.0f);
	glRotatef(90, 1, 0, 0);
	gluCylinder(qobj, 0.025, 0.025, 0.075, 16, 10);
	gluDeleteQuadric(qobj);
	glPopMatrix();

	// NECK
	glPushMatrix();
    GLUquadricObj * qobj2 = gluNewQuadric();
    glColor3f(0.5f,0.7f,0.7f);
    glTranslatef(0.0, 0.25, 0.0);
	glRotatef(90, 1, 0, 0);
	gluCylinder(qobj2, 0.035, 0.035, 0.075, 16, 10);
	gluDeleteQuadric(qobj2);
	glPopMatrix();

	// BACK TRIANGLES
    glPushMatrix();
    glRotatef(bodyAngle, 0, 1, 0);
    glBegin(GL_TRIANGLES);
    	glColor3f(1, 1, 0);
    	glNormal3f(0, 0, -1);
    	glVertex3f(0, 0, -0.126);
    	glVertex3f(-0.05, -0.05, -0.126);
    	glVertex3f(0.05, -0.05, -0.126);

		glNormal3f(0, 0, -1);
    	glVertex3f(0, 0.1, -0.126);
    	glVertex3f(-0.05, 0.05, -0.126);
    	glVertex3f(0.05, 0.05, -0.126);
    glEnd();

	// FRONT SQUARE
    glBegin(GL_QUADS);
    	glNormal3f(0, 0, 1);
    	glVertex3f(0.0625, -0.1, 0.126);
    	glVertex3f(0.0625, 0.1, 0.126);
    	glVertex3f(-0.0625, 0.1, 0.126);
    	glVertex3f(-0.0625, -0.1, 0.126);
    glEnd();
    glPopMatrix();

glPushMatrix();
		// EYES
glRotatef(headAngle, 0, 1, 0);
    this->drawFilledCircle(-0.03, 0.32, 0.0626, 0.02);
    this->drawFilledCircle(0.03, 0.32, 0.0626, 0.02);
	glPopMatrix();

    angle = (angle + 2) >= 360 ? 0 : angle + 2;
}
float Robot::getBodyX()
{
	return this->bodyX;
}
float Robot::getBodyY()
{
	return this->bodyY;
}
float Robot::getBodyZ()
{
	return this->bodyZ;
}
void Robot::setBodyX(float x)
{
	this->bodyX = x;
}
void Robot::setBodyY(float y)
{
	this->bodyY = y;
}
void Robot::setBodyZ(float z)
{
	this->bodyZ = z;
}
//////////////////////////////////////////////////////
float Robot::getHeadX()
{
	return this->headX;
}
float Robot::getHeadY()
{
	return this->headY;
}
float Robot::getHeadZ()
{
	return this->headZ;
}
void Robot::setHeadX(float x)
{
	this->headX = x;
}
void Robot::setHeadY(float y)
{
	this->headY = y;
}
void Robot::setHeadZ(float z)
{
	this->headZ = z;
}
Robot::~Robot()
{
	
}
