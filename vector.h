#ifndef VECTOR_H
#define VECTOR_H

#include <math.h>

class Vector {

  public:
    float X,Y,Z;

    Vector(void);
    Vector(float X, float Y, float Z);
    ~Vector(void);
    float Length();
    Vector Normalize();

};

#endif
