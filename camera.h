#ifndef CAMERA_H
#define CAMERA_H

#include <math.h>
#include "vector.h"

class Camera {
  private:
    Vector look;
    Vector at;
    Vector up;
    float pitch, yaw;
    double toRad(int deg);

  public:
    Camera(float x, float y, float z, float atX, float atY, float atZ, float upX, float upY, float upZ);
    float getX();
    float getY();
    float getZ();
    float getAtX();
    float getAtY();
    float getAtZ();
    float getUpX();
    float getUpY();
    float getUpZ();
    float getPitch();
    float getYaw();

    void setX(float x);
    void setY(float y);
    void setZ(float z);
    void setAtX(float atX);
    void setAtY(float atY);
    void setAtZ(float atZ);
    void setUpX(float atX);
    void setUpY(float atY);
    void setUpZ(float atZ);
    void setPitch(float p);
    void setYaw(float y);
};

#endif // CAMERA_H
