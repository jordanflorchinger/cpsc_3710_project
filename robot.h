#ifndef ROBOT_H
#define ROBOT_H

#include "camera.h"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#define PI 3.141592654

class Robot {
	private:
		Camera &camera;
		float bodyX, bodyY, bodyZ;
		float headX, headY, headZ;
		float angle;

	public:
		float headAngle, bodyAngle;

		float countUp, countLeft;
		int sCount, dCount;
		bool facingLeft, facingForward;
		bool facingBack, facingRight;

		Robot(Camera &cam);
		void renderRobot();
		void drawFilledCircle(GLfloat x, GLfloat y, GLfloat z, GLfloat radius);
		////////////////////////////////
		float getBodyX();
		float getBodyY();
		float getBodyZ();
		
		void setBodyX(float x);
		void setBodyY(float y);
		void setBodyZ(float z);
		///////////////////////////////
		float getHeadX();
		float getHeadY();
		float getHeadZ();
		
		void setHeadX(float x);
		void setHeadY(float y);
		void setHeadZ(float z);
		///////////////////////////////
		
		
		~Robot();
};

#endif //ROBOT_H
