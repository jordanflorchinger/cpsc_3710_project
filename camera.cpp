#include "camera.h"
#include <iostream>

Camera::Camera(float x, float y, float z, float atX, float atY, float atZ, float upX, float upY, float upZ) {
  look = Vector(x, y, z);
  at = Vector(atX, atY, atZ);
  up = Vector(upX, upY, upZ);
  yaw = 0;
  pitch = 0;
}

double Camera::toRad(int deg) {
  return (deg * M_PI) / 180;
}

float Camera::getPitch() {
  return pitch;
}

float Camera::getYaw() {
  return yaw;
}

void Camera::setYaw(float y) {
  yaw = y;
}

void Camera::setPitch(float p) {
  pitch = p;
}

float Camera::getX() {
  return look.X;
}

float Camera::getY() {
  return look.Y;
}

float Camera::getZ() {
  return look.Z;
}

float Camera::getAtX() {
  return at.X;
}

float Camera::getAtY() {
  return at.Y;
}

float Camera::getAtZ() {
  return at.Z;
}

float Camera::getUpX() {
  return up.X;
}

float Camera::getUpY() {
  return up.Y;
}

float Camera::getUpZ() {
  return up.Z;
}

void Camera::setX(float x) {
  look.X = x;
}

void Camera::setY(float y) {
  look.Y = y;
}

void Camera::setZ(float z) {
  look.Z = z;
}

void Camera::setAtX(float atX) {
  at.X = atX;
}

void Camera::setAtY(float atY) {
  at.Y = atY;
}

void Camera::setAtZ(float atZ) {
  at.Z = atZ;
}

void Camera::setUpX(float upX) {
  up.X = upX;
}

void Camera::setUpY(float upY) {
  up.Y = upY;
}

void Camera::setUpZ(float upZ) {
  up.Z = upZ;
}
