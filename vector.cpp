#include "vector.h"

Vector::Vector(void) {
}

Vector::Vector(float X, float Y, float Z) {
  this->X = X;
  this->Y = Y;
  this->Z = Z;
}

float Vector::Length() {
  return sqrt(X * X + Y * Y + Z * Z);
}

Vector Vector::Normalize() {
  Vector vector;
  float length = this->Length();

  if(length != 0){
    vector.X = X/length;
    vector.Y = Y/length;
    vector.Z = Z/length;
  }

  return vector;
}

Vector::~Vector(void) {
}
