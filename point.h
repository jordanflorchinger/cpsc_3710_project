#ifndef POINT_H
#define POINT_H

#include <math.h>

class Point {
  private:
    int _x, _y;
  public:
    Point(int x, int y);
    int getX();
    int getY();
    void setX(int x);
    void setY(int y);
    float distance(Point p);

    bool operator<(const Point &p) const {
      return _x < p._x || (_x == p._x && _y < p._y);
    }
};

#endif // POINT_H
