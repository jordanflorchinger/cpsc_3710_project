#define SIZE 4
#define WIDTH 20
#define HEIGHT 20

#include "landscape.h"

Landscape::Landscape(Camera &cam) : camera(cam) {
  buildingCount = 0;
  srand(time(NULL));
  for (unsigned int y = 0; y < HEIGHT; y++) {
    for (unsigned int x = 0; x < WIDTH; x++) {
      Point p(((x * 5) + (SIZE / 2) + 1), ((y * 5) + (SIZE / 2) + 1));
      blockMap.insert(std::pair<Point, std::vector<Building> >(p, this->generateBuildings(p, SIZE, 8)));
    }
  }
}

// Pass in the center point of the square so: blockVector.at(x);
std::vector<Building> Landscape::generateBuildings(Point p, float width, int count) {
  bool flag = false;
  std::vector<Building> buildings;
  buildings.push_back(Building(0, 0, 0, 0, 0, 0, 0));
  while (buildings.size() < count) {
    float x = this->nextInt(-2, 1);
    float y = this->nextInt(-2, 1);
    for (int i = 0; i < buildings.size(); i++) {
      Building test = buildings.at(i);
      if (test.getX() == p.getX() + x && test.getY() == p.getY() + y) {
        flag = true;
      }
    }
    if (flag == false) {
      float color = this->nextFloat(0, 1);
      Building tmp(p.getX() + x, p.getY() + y, this->nextFloat(0.6, 2), this->nextFloat(0.7, 1.0), this->nextFloat(0, 1), this->nextFloat(0, 1), this->nextFloat(0, 1));
      buildings.push_back(tmp);
    }
    flag = false;
  }
  return buildings;
}

void Landscape::remove(int block, int building) {
  std::vector<Point> keySet;
  for (std::map<Point, std::vector<Building> >::iterator it = blockMap.begin(); it != blockMap.end(); ++it) {
    keySet.push_back(it->first);
  }
  if (blockMap.count(keySet.at(block)) > 0 &&
        blockMap.at(keySet.at(block)).size() > 1) {
    buildingCount++;
    blockMap.at(keySet.at(block)).erase(blockMap.at(keySet.at(block)).begin() + building);
  }
}

int Landscape::distance(Point player, int block, int building) {
  std::vector<Point> keySet;
  for (std::map<Point, std::vector<Building> >::iterator it = blockMap.begin(); it != blockMap.end(); ++it) {
    keySet.push_back(it->first);
  }
  if (blockMap.count(keySet.at(block)) > 0 &&
        blockMap.at(keySet.at(block)).size() > 1) {
    return blockMap.at(keySet.at(block)).at(building).getCenter().distance(player);
  }
  return -1;
}

int Landscape::cantorPair(int x, int y) {
  return ((x + y) * (x + y + 1)) / 2 + y;
}

Point Landscape::reverseCantor(int val) {
  int t = (int) floor((-1 + sqrt(1 + 8 * val)) / 2);
  return Point(t * (t + 3) / 2 - val, val - t * (t + 1) / 2);
}

/*
  The function in charge of redendering the Landscape

    - Future modifications?:
      - Distance from robot based rendering - more efficient?
*/
void Landscape::renderMap(GLenum mode) {
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glPushAttrib(GL_ENABLE_BIT);

  gluLookAt(camera.getX(), camera.getY(), camera.getZ(),
            camera.getAtX(), camera.getAtY(), camera.getAtZ(),
            camera.getUpX(), camera.getUpY(), camera.getUpZ());

  std::vector<Point> keySet;
  for (std::map<Point, std::vector<Building> >::iterator it = blockMap.begin(); it != blockMap.end(); ++it) {
    keySet.push_back(it->first);
  }
  for(int i = 0; i < keySet.size(); i++) {
    Point p = keySet.at(i);

    if (blockMap.count(p) >= 1) {
      std::vector<Building> bs = blockMap.at(p);
      for (size_t j = 0; j < bs.size(); j++) {
        Building b = bs.at(j);
        if (b.getHeight() == 0) {
          continue;
        }
        if (mode == GL_SELECT) {
          glLoadName(this->cantorPair(i, j));
        }
        glBegin(GL_QUADS);
          glColor3f(b.getR(), b.getG(), b.getB());
          // if (mode == GL_SELECT) {
          //   glLoadName(this->cantorPair(i, j));
          // }
          glNormal3f(0, -1, 0);
          glVertex3f(b.getX(), 0.001, b.getY());
          glVertex3f(b.getX() + b.getWidth(), 0.001, b.getY());
          glVertex3f(b.getX() + b.getWidth(), 0.001, b.getY() + b.getWidth());
          glVertex3f(b.getX(), 0.001, b.getY() + b.getWidth());

          // if (mode == GL_SELECT) {
          //   glLoadName(this->cantorPair(i, j));
          // }
          glNormal3f(0, 1, 0);
          glVertex3f(b.getX(), b.getHeight(), b.getY());
          glVertex3f(b.getX() + b.getWidth(), b.getHeight(), b.getY());
          glVertex3f(b.getX() + b.getWidth(), b.getHeight(), b.getY() + b.getWidth());
          glVertex3f(b.getX(), b.getHeight(), b.getY() + b.getWidth());

          // if (mode == GL_SELECT) {
          //   glLoadName(this->cantorPair(i, j));
          // }
          glNormal3f(-1, 0, 0);
          glVertex3f(b.getX(), 0.001, b.getY());
          glVertex3f(b.getX(), b.getHeight(), b.getY());
          glVertex3f(b.getX(), b.getHeight(), b.getY() + b.getWidth());
          glVertex3f(b.getX(), 0.001, b.getY() + b.getWidth());

          // if (mode == GL_SELECT) {
          //   glLoadName(this->cantorPair(i, j));
          // }
          glNormal3f(1, 0, 0);
          glVertex3f(b.getX() + b.getWidth(), 0.001, b.getY() + b.getWidth());
          glVertex3f(b.getX() + b.getWidth(), b.getHeight(), b.getY() + b.getWidth());
          glVertex3f(b.getX() + b.getWidth(), b.getHeight(), b.getY());
          glVertex3f(b.getX() + b.getWidth(), 0.001, b.getY());

          // if (mode == GL_SELECT) {
          //   glLoadName(this->cantorPair(i, j));
          // }
          glNormal3f(0, 0, -1);
          glVertex3f(b.getX(), 0.001, b.getY());
          glVertex3f(b.getX() + b.getWidth(), 0.001, b.getY());
          glVertex3f(b.getX() + b.getWidth(), b.getHeight(), b.getY());
          glVertex3f(b.getX(), b.getHeight(), b.getY());

          // if (mode == GL_SELECT) {
          //   glLoadName(this->cantorPair(i, j));
          // }
          glNormal3f(0, 0, 1);
          glVertex3f(b.getX(), 0.001, b.getY() + b.getWidth());
          glVertex3f(b.getX(), b.getHeight(), b.getY() + b.getWidth());
          glVertex3f(b.getX() + b.getWidth(), b.getHeight(), b.getY() + b.getWidth());
          glVertex3f(b.getX() + b.getWidth(), 0.001, b.getY() + b.getWidth());
        glEnd();
      }
    }

    int small = SIZE / 2;
    glBegin(GL_QUADS);
      glColor3f(0.5, 0.5, 0.5);
      glNormal3f(0, 1, 0);
      glVertex3f(p.getX() + small, 0.001, p.getY() + small);
      glVertex3f(p.getX() - small, 0.001, p.getY() + small);
      glVertex3f(p.getX() - small, 0.001, p.getY() - small);
      glVertex3f(p.getX() + small, 0.001, p.getY() - small);
    glEnd();
  }

  for (int x = 0; x < WIDTH + 1; x++) {
    glLineWidth(0.1);
    glColor3f(1, 0.9, .01);
    glLineStipple(1, 0x00FF);
    glEnable(GL_LINE_STIPPLE);
    glBegin(GL_LINES);
      glVertex3f((x * 5) + 0.5, 0.001, 0.5);
      glVertex3f((x * 5) + 0.5, 0.001, WIDTH * 5 + 0.5);
    glEnd();
  }

  for (int y = 0; y < HEIGHT + 1; y++) {
    glLineWidth(0.1);
    glColor3f(1, 0.9, .01);
    glLineStipple(1, 0x00FF);
    glEnable(GL_LINE_STIPPLE);
    glBegin(GL_LINES);
      glVertex3f(0.5, 0.001, (y * 5) + 0.5);
      glVertex3f(HEIGHT * 5 + 0.5, 0.001, (y * 5) + 0.5);
    glEnd();
  }

  glBegin(GL_QUADS);
    glColor3f(0.2, 0.2, 0.2);
    glNormal3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 5 * HEIGHT + 1);
    glVertex3f(5 * WIDTH + 1, 0, 5 * HEIGHT + 1);
    glVertex3f(5 * WIDTH + 1, 0, 0);
  glEnd();
}

/*
  Returns a float between the two given floats
*/
float Landscape::nextFloat(float a, float b) {
    float random = ((float) rand()) / (float) RAND_MAX;
    float diff = b - a;
    float r = random * diff;
    return a + r;
}

int Landscape::nextInt(int a, int b) {
  return rand() % (b - a + 1) + a;
}

Landscape::~Landscape() {

}
