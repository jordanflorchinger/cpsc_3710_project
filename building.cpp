#include "building.h"

Building::Building(float x, float y, float height, float width, float r, float g, float b) {
  _x = x;
  _y = y;
  _height = height;
  _width = width;
  _r = r;
  _g = g;
  _b = b;
}

Point Building::getPoint() {
  return Point(_x, _y);
}

Point Building::getCenter() {
  return Point(_x + _width / 2, _y + _width / 2);
}

float Building::getX() {
  return _x;
}

float Building::getY() {
  return _y;
}

float Building::getWidth() {
  return _width;
}

float Building::getHeight() {
  return _height;
}

float Building::getR() {
  return _r;
}

float Building::getG() {
  return _g;
}

float Building::getB() {
  return _b;
}

Building::~Building() {

}
