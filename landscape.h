#ifndef LANDSCAPE_H
#define LANDSCAPE_H

#include <vector>
#include <map>
#include "point.h"
#include "building.h"
#include "camera.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <iostream>
#include <ctime>
#include <math.h>

class Landscape {
  private:
    float playerX, playerY;
    Camera &camera;

  public:
    int buildingCount;
    std::map<Point, std::vector<Building> > blockMap;
    Landscape(Camera &cam);
    void renderMap(GLenum mode);
    std::vector<Building> generateBuildings(Point p, float width, int count);
    float nextFloat(float a, float b);
    int nextInt(int a, int b);

    int cantorPair(int x, int y);
    Point reverseCantor(int val);

    int distance(Point player, int block, int building);
    void remove(int block, int building);

    ~Landscape();
};

#endif
