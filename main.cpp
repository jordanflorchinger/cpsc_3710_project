#define BUF_SIZE 512

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <limits.h>
#include <string.h>

#include "landscape.h"
#include "robot.h"
#include <sstream>
#include <string>

using namespace std;

int Window_Width = 1280, Window_Height = 720;

Camera camera(0.5, 1, -4, 0.5, 1, 1, 0, 1, 0);
Camera camReset(0.5, 1, -4, 0.5, 1, 1, 0, 1, 0);
Landscape landscape(camera);
Robot robot(camera);
Robot Reset(camReset);
bool Pause = false;
int boundaryX = 100;
int boundaryY = 100;

static void drawString(float x, float y, void *font, const char *string){
   const char *c;
   glRasterPos2f(x, y);
   for(c=string; *c != '\0'; c++){
      glutBitmapCharacter(font, *c);
   }
}

void pauseString()
{
   if(Pause == true){
      glLoadIdentity();
      glMatrixMode(GL_PROJECTION);
      glPushMatrix();
      glLoadIdentity();
      glOrtho(0,Window_Width,0,Window_Height,-1.0,1.0);
      glColor4f(0.6,1.0,0.6,1.00);
      drawString(Window_Width/2 - 25, Window_Height/2 - 90, GLUT_BITMAP_HELVETICA_18, "pause");
      glPopMatrix();
   }
}

void resetRobot(Robot &robot, Robot Reset)
{
   robot.setBodyX(Reset.getBodyX());
   robot.setBodyY(Reset.getBodyY());
   robot.setBodyZ(Reset.getBodyZ());
   robot.setHeadX(Reset.getHeadX());
   robot.setHeadY(Reset.getHeadY());
   robot.setHeadZ(Reset.getHeadZ());
   robot.headAngle = 0;
   robot.countUp = robot.countLeft = 0;
   robot.sCount = robot.dCount = 0;
   robot.facingLeft = robot.facingBack = robot.facingRight = false;
   robot.facingForward = true;
   

   camera.setX(camReset.getX());
   camera.setY(camReset.getY());
   camera.setZ(camReset.getZ());
   camera.setAtX(camReset.getAtX());
   camera.setAtY(camReset.getAtY());
   camera.setAtZ(camReset.getAtZ());
   camera.setUpX(camReset.getUpX());
   camera.setUpY(camReset.getUpY());
   camera.setUpZ(camReset.getUpZ());

}

void swapCount(float &count1, float &count2)
{
	float temp;
	temp = count1;
	count1 = count2;
	count2 = temp;
}

void processHits(GLint hits, GLuint buffer[]) {
	if (hits == 0) {
		return;
	}
	unsigned int i, j;
	GLuint names, *ptr, minZ,*ptrNames, numberOfNames;
	
	ptr = (GLuint *) buffer;
	minZ = 0xffffffff;
	for (i = 0; i < hits; i++) {
	 names = *ptr;
	 ptr++;
	 if (*ptr < minZ) {
		 numberOfNames = names;
		 minZ = *ptr;
		 ptrNames = ptr+2;
	 }
	
	 ptr += names+2;
	}
	ptr = ptrNames;
	for (j = 0; j < numberOfNames; j++,ptr++) {
		Point r = landscape.reverseCantor(*ptr);
		float test = landscape.distance(Point(robot.countLeft, robot.countUp), r.getX(), r.getY());
		if (test < 15) {
			landscape.remove(r.getX(), r.getY());
			std::cout << "Building Count: " << landscape.buildingCount << std::endl;
		}
	}
}

void CallBackRenderScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	landscape.renderMap(GL_RENDER);
	robot.renderRobot();
	pauseString();


	glPushMatrix();
	glOrtho(0,Window_Width,0,Window_Height,-1.0,1.0);
	glColor4f(0,1.0,1.0,1.00);
	std::ostringstream oss;
	oss << landscape.buildingCount;
	std::string str = "Buildings: " + oss.str();
	drawString(Window_Width / 2, Window_Height, GLUT_BITMAP_HELVETICA_18, str.c_str());
	glPopMatrix();

	glFlush();
	glutSwapBuffers();
}

void CallBackMouseEvent(int button, int state, int x, int y) {
   	GLuint selectBuf[BUF_SIZE];
   	GLint hits;
   	GLint viewport[4];

	if(Pause == false){
	   if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	   {
	      glGetIntegerv(GL_VIEWPORT, viewport);
	      glSelectBuffer(BUF_SIZE, selectBuf);
	      glRenderMode(GL_SELECT);
	      glInitNames();
	      glPushName(0);
	      glMatrixMode(GL_PROJECTION);
	      glPushMatrix();
	      glLoadIdentity();


	      gluPickMatrix((GLdouble) x, (GLdouble) (viewport[3] - y), 1.0, 1.0, viewport);

	      gluPerspective(45.0f,(GLfloat)Window_Width/(GLfloat)Window_Height,0.1f,100.0f);

	      landscape.renderMap(GL_SELECT);

	      glMatrixMode(GL_PROJECTION);
	      glPopMatrix();
	      glFlush();
	      hits = glRenderMode(GL_RENDER);

	      processHits(hits, selectBuf);

	      glutPostRedisplay();

	   }
	   else if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	   {


	   }
	}
}

void CallBackKeyPressed(unsigned char key, int x, int y) {
   switch (key) {
      case 'z':
	 if(Pause == true)
	    break;
	 	if(robot.countUp < 100 && (int)robot.countLeft % 5 == 0 && robot.facingForward == true)
	 	{
		 	robot.setBodyZ(robot.getBodyZ() - 1);
		 	robot.setHeadZ(robot.getHeadZ() - 1);
		 	camera.setZ(camera.getZ() + 1);
		 	camera.setAtZ(camera.getAtZ() + 1);
		 	robot.countUp++;
	 	}
		else if(robot.countLeft < 100 && (int)robot.countUp % 5 == 0 && robot.facingLeft == true)
		{
			robot.setBodyZ(robot.getBodyZ() - 1);
		 	robot.setHeadZ(robot.getHeadZ() - 1);
		 	camera.setX(camera.getX() + 1);
		 	camera.setAtX(camera.getAtX() + 1);
		 	robot.countLeft++;
		}
		else if(robot.countUp > 0 && (int)robot.countLeft % 5 == 0 && robot.facingBack == true)
	 	{
		 	robot.setBodyZ(robot.getBodyZ() - 1);
		 	robot.setHeadZ(robot.getHeadZ() - 1);
		 	camera.setZ(camera.getZ() - 1);
		 	camera.setAtZ(camera.getAtZ() - 1);
		 	robot.countUp--;
	 	}
	 	else if(robot.countLeft > 0 && (int)robot.countUp % 5 == 0 && robot.facingRight == true)
		{
			robot.setBodyZ(robot.getBodyZ() - 1);
		 	robot.setHeadZ(robot.getHeadZ() - 1);
		 	camera.setX(camera.getX() - 1);
		 	camera.setAtX(camera.getAtX() - 1);
		 	robot.countLeft--;
		}
	 	break;
      case 'q':
	 if(Pause == true)
	    break;
	 	robot.bodyAngle += 90;
		robot.headAngle += 90;
		if(robot.sCount % 4 == 0 && (int)robot.countUp % 5 == 0)//left
		{
			robot.sCount++;
			robot.dCount = 3;
			robot.facingForward = false;
			robot.facingRight = false;
			robot.facingBack = false;
			robot.facingLeft = true;
		 	camera.setX(-4 + robot.countLeft);
		 	camera.setZ(0.5 + robot.countUp);
			camera.setAtX(1 + robot.countLeft);
			camera.setAtZ(0.5 + robot.countUp);
			robot.setBodyX(robot.getBodyX() + 1 + robot.countUp + robot.countLeft);
			robot.setHeadX(robot.getHeadX() + 1 + robot.countUp + robot.countLeft);
			robot.setBodyZ(robot.getBodyZ() + robot.countUp - robot.countLeft);
			robot.setHeadZ(robot.getHeadZ() + robot.countUp - robot.countLeft);
		}
		else if(robot.sCount % 4 == 1 && (int)robot.countLeft % 5 == 0)//back
		{
			robot.sCount++;
			robot.dCount = 2;
			robot.facingLeft = false;
			robot.facingForward = false;
			robot.facingRight = false;
			robot.facingBack = true;
		 	camera.setX(0.5 + robot.countLeft);
		 	camera.setZ(4 + 1 + robot.countUp);
			camera.setAtX(0.5 + robot.countLeft);
			camera.setAtZ(-1 + robot.countUp);
			robot.setBodyX(robot.getBodyX() - robot.countUp + robot.countLeft);
			robot.setHeadX(robot.getHeadX() - robot.countUp + robot.countLeft);
			robot.setBodyZ(robot.getBodyZ() + 1 + robot.countUp + robot.countLeft);
			robot.setHeadZ(robot.getHeadZ() + 1 + robot.countUp + robot.countLeft);
		}
		else if(robot.sCount % 4 == 2 && (int)robot.countUp % 5 == 0)//right
		{
			robot.sCount++;
			robot.dCount = 1;
			robot.facingLeft = false;
			robot.facingForward = false;
			robot.facingBack = false;
			robot.facingRight = true;
		 	camera.setX(4 + 1 + robot.countLeft);
		 	camera.setZ(0.5 + robot.countUp);
			camera.setAtX(-1 + robot.countLeft);
			camera.setAtZ(0.5 + robot.countUp);
			robot.setBodyX(robot.getBodyX() - 1 - robot.countUp - robot.countLeft);
			robot.setHeadX(robot.getHeadX() - 1 - robot.countUp - robot.countLeft);
			robot.setBodyZ(robot.getBodyZ() - robot.countUp + robot.countLeft);
			robot.setHeadZ(robot.getHeadZ() - robot.countUp + robot.countLeft);
		}
		else if(robot.sCount % 4 == 3 && (int)robot.countLeft % 5 == 0)//forward
		{
			robot.sCount++;
			robot.dCount = 0;
			robot.facingRight = false;
			robot.facingBack = false;
			robot.facingLeft = false;
			robot.facingForward = true;
		 	camera.setX(0.5 + robot.countLeft);
		 	camera.setZ(-4 + robot.countUp);
			camera.setAtX(0.5 + robot.countLeft);
			camera.setAtZ(1 + robot.countUp);
			robot.setBodyX(robot.getBodyX() + robot.countUp - robot.countLeft);
			robot.setHeadX(robot.getHeadX() + robot.countUp - robot.countLeft);
			robot.setBodyZ(robot.getBodyZ() - 1 - robot.countUp - robot.countLeft);
			robot.setHeadZ(robot.getHeadZ() - 1 - robot.countUp - robot.countLeft);
		}
		break;
      case 'a':
	 if(Pause == true)
	    break;
	 	robot.bodyAngle -= 90;
		robot.headAngle -= 90;
	 	if(robot.dCount % 4 == 0 && (int)robot.countUp % 5 == 0)//right
		{
			robot.dCount++;
			robot.sCount = 3;
			robot.facingForward = false;
			robot.facingLeft = false;
			robot.facingBack = false;
			robot.facingRight = true;
		 	camera.setX(4 + 1 + robot.countLeft);
		 	camera.setZ(0.5 + robot.countUp);
			camera.setAtX(-1 + robot.countLeft);
			camera.setAtZ(0.5 + robot.countUp);
			robot.setBodyX(robot.getBodyX() - robot.countUp + robot.countLeft);
			robot.setHeadX(robot.getHeadX() - robot.countUp + robot.countLeft);
			robot.setBodyZ(robot.getBodyZ() + 1 + robot.countUp + robot.countLeft);
			robot.setHeadZ(robot.getHeadZ() + 1 + robot.countUp + robot.countLeft);
		}
		else if(robot.dCount % 4 == 1 && (int)robot.countLeft % 5 == 0)//back
		{
			robot.dCount++;
			robot.sCount = 2;
			robot.facingRight = false;
			robot.facingForward - false;
			robot.facingLeft = false;
			robot.facingBack = true;
		 	camera.setX(0.5 + robot.countLeft);
		 	camera.setZ(4 + 1 + robot.countUp);
			camera.setAtX(0.5 + robot.countLeft);
			camera.setAtZ(-1 + robot.countUp);
			robot.setBodyX(robot.getBodyX() + 1 + robot.countUp + robot.countLeft);
			robot.setHeadX(robot.getHeadX() + 1 + robot.countUp + robot.countLeft);
			robot.setBodyZ(robot.getBodyZ() + robot.countUp - robot.countLeft);
			robot.setHeadZ(robot.getHeadZ() + robot.countUp - robot.countLeft);
		}
		else if(robot.dCount % 4 == 2 && (int)robot.countUp % 5 == 0)//left
		{
			robot.dCount++;
			robot.sCount = 1;
			robot.facingRight = false;
			robot.facingBack = false;
			robot.facingForward = false;
			robot.facingLeft = true;
		 	camera.setX(-4 + robot.countLeft);
		 	camera.setZ(0.5 + robot.countUp);
			camera.setAtX(1 + robot.countLeft);
			camera.setAtZ(0.5 + robot.countUp);
			robot.setBodyX(robot.getBodyX() + robot.countUp - robot.countLeft);
			robot.setHeadX(robot.getHeadX() + robot.countUp - robot.countLeft);
			robot.setBodyZ(robot.getBodyZ() - 1 - robot.countUp - robot.countLeft);
			robot.setHeadZ(robot.getHeadZ() - 1 - robot.countUp - robot.countLeft);
		}
		else if(robot.dCount % 4 == 3 && (int)robot.countLeft % 5 == 0)//forward
		{
			robot.dCount++;
			robot.sCount = 0;
			robot.facingRight = false;
			robot.facingBack = false;
			robot.facingLeft = false;
			robot.facingForward = true;
		 	camera.setX(0.5 + robot.countLeft);
		 	camera.setZ(-4 + robot.countUp);
			camera.setAtX(0.5 + robot.countLeft);
			camera.setAtZ(1 + robot.countUp);
			robot.setBodyX(robot.getBodyX() - 1 - robot.countUp - robot.countLeft);
			robot.setHeadX(robot.getHeadX() - 1 - robot.countUp - robot.countLeft);
			robot.setBodyZ(robot.getBodyZ() - robot.countUp + robot.countLeft);
			robot.setHeadZ(robot.getHeadZ() - robot.countUp + robot.countLeft);
		}
		break;

      case 'p':
      	Pause = !Pause;
	 	break;
      case 'r':
	 if(Pause == false && (robot.countUp == 0 || robot.countLeft == 0 || robot.countLeft == boundaryX || robot.countUp == boundaryY)){
	    resetRobot(robot, Reset);
	 }
	 break;
      

   }
}
///////////////// SPECIAL KEY FUNC //////////////////////////
void CallBackSpecialUp(int key, int x, int y)
{
	if(!Pause)
	{
		switch (key)
		{
			case GLUT_KEY_F2:
				robot.headAngle -= 110;
				break;
			case GLUT_KEY_F3:
				robot.headAngle += 110;
				break;
			case GLUT_KEY_F6:
				if(robot.facingForward == true)
				{
					robot.headAngle -= 45;
					camera.setX(camera.getX() - 0.5);
					camera.setAtX(camera.getAtX() - 0.5);
					break;
				}
				else if(robot.facingRight == true)
				{
					robot.headAngle -= 45;
					camera.setZ(camera.getZ() - 0.5);
					camera.setAtZ(camera.getAtZ() - 0.5);
					break;
				}
				else if(robot.facingLeft == true)
				{
					robot.headAngle += 45;
					camera.setZ(camera.getZ() + 0.5);
					camera.setAtZ(camera.getAtZ() + 0.5);
					break;
				}
				if(robot.facingBack == true)
				{
					robot.headAngle += 45;
					camera.setX(camera.getX() + 0.5);
					camera.setAtX(camera.getAtX() + 0.5);
					break;
				}
			case GLUT_KEY_F5:
				if(robot.facingForward == true)
				{
					robot.headAngle += 45;
					camera.setX(camera.getX() + 0.5);
					camera.setAtX(camera.getAtX() + 0.5);
					break;
				}
				else if(robot.facingRight == true)
				{
					robot.headAngle += 45;
					camera.setZ(camera.getZ() + 0.5);
					camera.setAtZ(camera.getAtZ() + 0.5);
					break;
				}
				else if(robot.facingLeft == true)
				{
					robot.headAngle -= 45;
					camera.setZ(camera.getZ() - 0.5);
					camera.setAtZ(camera.getAtZ() - 0.5);
					break;
				}
				if(robot.facingBack == true)
				{
					robot.headAngle -= 45;
					camera.setX(camera.getX() - 0.5);
					camera.setAtX(camera.getAtX() - 0.5);
					break;
				}
		}
	}
}
void CallBackSpecialKey(int key, int x, int y)
{
	if(!Pause)
	{
		switch (key)
		{
			case GLUT_KEY_F2:
				robot.headAngle += 110;
				break;
			case GLUT_KEY_F3:
				robot.headAngle -= 110;
				break;
			case GLUT_KEY_F6:
				if(robot.facingForward == true)
				{
					robot.headAngle += 45;
					camera.setX(camera.getX() + 0.5);
					camera.setAtX(camera.getAtX() + 0.5);
					break;
				}
				else if(robot.facingRight == true)
				{
					robot.headAngle += 45;
					camera.setZ(camera.getZ() + 0.5);
					camera.setAtZ(camera.getAtZ() + 0.5);
					break;
				}
				else if(robot.facingLeft == true)
				{
					robot.headAngle -= 45;
					camera.setZ(camera.getZ() - 0.5);
					camera.setAtZ(camera.getAtZ() - 0.5);
					break;
				}
				if(robot.facingBack == true)
				{
					robot.headAngle -= 45;
					camera.setX(camera.getX() - 0.5);
					camera.setAtX(camera.getAtX() - 0.5);
					break;
				}
			case GLUT_KEY_F5:
				if(robot.facingForward == true)
				{
					robot.headAngle -= 45;
					camera.setX(camera.getX() - 0.5);
					camera.setAtX(camera.getAtX() - 0.5);
					break;
				}
				else if(robot.facingRight == true)
				{
					robot.headAngle -= 45;
					camera.setZ(camera.getZ() - 0.5);
					camera.setAtZ(camera.getAtZ() - 0.5);
					break;
				}
				else if(robot.facingLeft == true)
				{
					robot.headAngle += 45;
					camera.setZ(camera.getZ() + 0.5);
					camera.setAtZ(camera.getAtZ() + 0.5);
					break;
				}
				if(robot.facingBack == true)
				{
					robot.headAngle += 45;
					camera.setX(camera.getX() + 0.5);
					camera.setAtX(camera.getAtX() + 0.5);
					break;
				}
		}
	}
}
void CallBackResizeScene(int Width, int Height) {
   if (Height == 0)
      Height = 1;

   glViewport(0, 0, Width, Height);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(45.0f,(GLfloat)Width/(GLfloat)Height,0.1f,100.0f);

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   Window_Width  = Width;
   Window_Height = Height;
}

void init (int width, int height) {
	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_shininess[] = { 50.0 };
	GLfloat light_position[] = { -5, 5, -5, 0.0 };

	GLfloat mat_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };

	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT1, GL_POSITION, light_position);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, mat_diffuse);

	glEnable(GL_LIGHTING);
	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glClearColor(0.1f, 0.1f, 0.1f, 0.0f);

	glClearDepth(1.0);
	glDepthFunc(GL_LESS);

	glDepthMask(GL_TRUE);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_DEPTH_TEST);

	glShadeModel(GL_SMOOTH);
	CallBackResizeScene(width, height);

}

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
  glutInitWindowSize(Window_Width, Window_Height);
  glutCreateWindow("PROJECT");

  glutIdleFunc(&CallBackRenderScene);
  glutReshapeFunc(&CallBackResizeScene);
  glutDisplayFunc(&CallBackRenderScene);
  glutKeyboardFunc(&CallBackKeyPressed);
  glutSpecialFunc(&CallBackSpecialKey);
  glutSpecialUpFunc(&CallBackSpecialUp);
  glutMouseFunc(&CallBackMouseEvent);

  init(Window_Width, Window_Height);
  glutMainLoop();
  return 1;
}
