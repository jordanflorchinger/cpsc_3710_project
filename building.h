#ifndef BUILDING_H
#define BUILDING_H

#include "point.h"

class Building {
  private:
    float _x, _y;
    float _height;
    float _width;
    float _r, _g, _b;
  public:
    Building(float x, float y, float height, float width, float r, float g, float b);
    float getX();
    float getY();
    float getWidth();
    float getHeight();
    float getR();
    float getG();
    float getB();
    Point getPoint();
    Point getCenter();

    ~Building();
};

#endif
